//
//  GameScene.swift
//  ios2d-Berzerk
//
//  Created by Parrot on 2019-06-12.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {

    // sprites
    var player:SKNode!
     var enemiesArray:[SKNode] = []
    
    
    
    // GAME STAT SPRITES
    let livesLabel = SKLabelNode(text: "Lives: ")
    let scoreLabel = SKLabelNode(text: "Score: ")
    
    
    // GAME STATISTIC VARIABLES
    var lives = 2           // for testing, use a small number
    var score = 0
    
    
    
    
    override func didMove(to view: SKView) {
        // Required for SKPhysicsContactDelegate
        self.physicsWorld.contactDelegate = self
        
        // get the sprites from SceneKit Editor
        self.player = self.childNode(withName: "player")
        
        
        // get the sprites from SceneKit Editor
        self.player = self.childNode(withName: "player")
        
       
        
        
        // make array of enemies
        let e1 = self.childNode(withName: "enemy1")
        let e2 = self.childNode(withName: "enemy2")
        let e3 = self.childNode(withName: "enemy3")
        let e4 = self.childNode(withName: "enemy4")
        self.enemiesArray.append(e1!)
        self.enemiesArray.append(e2!)
        self.enemiesArray.append(e3!)
        self.enemiesArray.append(e4!)
        
        // MARK: Add a lives label
        // ------------------------
        self.livesLabel.text = "Lives: \(self.lives)"
        self.livesLabel.fontName = "Avenir-Bold"
        self.livesLabel.fontColor = UIColor.yellow
        self.livesLabel.fontSize = 30;
        self.livesLabel.position = CGPoint(x:100,y:self.size.height-50)
        
        // MARK: Add a score label
        // ------------------------
        self.scoreLabel.text = "Score: \(self.score)"
        self.scoreLabel.fontName = "Avenir-Bold"
        self.scoreLabel.fontColor = UIColor.yellow
        self.scoreLabel.fontSize = 30;
        self.scoreLabel.position = CGPoint(x:230,y:self.size.height-50)
        
        addChild(self.livesLabel)
        addChild(self.scoreLabel)
        
       
        
    }
    
    // MARK: Function to notify us when two sprites touch
    func didBegin(_ contact: SKPhysicsContact) {

        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
     //    need to change win condition
        if (nodeA?.name == "player" && nodeB?.name == "exit") {
            print("Player reached exit")
            
            // GAME WIN!
            let messageLabel = SKLabelNode(text: "YOU WIN!")
            messageLabel.fontColor = UIColor.yellow
            messageLabel.fontSize = 60
            messageLabel.position.x = self.size.width/2
            messageLabel.position.y = self.size.height/2
            addChild(messageLabel)
        }
        
        
        // need to change lose condition
       
        
        

    }
    
    var timeOfLastUpdate:TimeInterval = -1;
   
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
       if (self.lose == false) {
         self.moveEnemy()
        }
        
        
        // MARK: Deal with Evil Otto
        // -----------------------------
        // this code handles the first time you call the update() function
        if (timeOfLastUpdate == -1) {
            // First time, set timeOfLastUpdate
            // to current time
            timeOfLastUpdate = currentTime
        }
        
        let timePassed = currentTime - timeOfLastUpdate
        
        if (timePassed >= 20) {
            print("5 SECONDS PASSED!")
            timeOfLastUpdate = currentTime
            
            
        }
        // ------ END TIME DETECTION -------------
        // collision between player and enemy
        for (i, enemy) in enemiesArray.enumerated() {
            if (self.player.intersects(enemy) == true) {
                print("COLLISION DETECTED!")
                // 1. increase the score
                self.score = self.score + 3
                // 2. remove the enemy from the scene
                print("Removing enemy at position: \(i)")
                // -------- add an egg -------
                let egg = SKSpriteNode(imageNamed: "Egg")
                egg.position = enemy.position
                // ---- 2a. remove from the array
                self.enemiesArray.remove(at: i)
                // ---- 2b. remove from scene (undraw the enemy)
                enemy.removeFromParent()
                
                addChild(egg)
            }
        }
        
        
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        if (touch == nil) {
            return
        }
        
        let mouseLocation = touch!.location(in:self)
        let spriteTouched = self.atPoint(mouseLocation)
     
        
        if (spriteTouched.name == "up") {
            print("UP PRESSED")
            self.player.position.y = self.player.position.y + 30
        }
        else if (spriteTouched.name == "down") {
            print("DOWN PRESSED")
            self.player.position.y = self.player.position.y - 30
        }
        else if (spriteTouched.name == "left") {
            print("LEFT PRESSED")
            self.player.position.x = self.player.position.x - 30
            if (self.player.position.x < 10)
            {
                self.player.position.x = self.size.width
            }
        }
        else if (spriteTouched.name == "right") {
            print("RIGHT PRESSED")
            self.player.position.x = self.player.position.x + 30
             if (self.player.position.x > self.size.width)
             {
                self.player.position.x = 10
            }
        }
    }
    
     var enemyDirection:String = "right"
    // MARK: Custom functions
   func moveEnemy() {
    
   for (i, enemy) in self.enemiesArray.enumerated() {
    print("Moving enemy : \(i)")
    // 2. Move enemy 10px each time
    if (self.enemyDirection == "right") {
        enemy.position.x = enemy.position.x + 3
        
    }
    else if (self.enemyDirection == "left") {
        enemy.position.x = enemy.position.x - 3
        
    }
    
    // 3. Detect when enemy hits the wall
    
    // R1:  Hit right wall, go left
    if (enemy.position.x >= self.size.width) {
        print("HIT THE WALL")
        self.enemyDirection = "left"
    }
    
    // R2:  Hight left wall, go right
    if (enemy.position.x <= 0) {
        self.enemyDirection = "right"
    }
    }// loop through all your enemy nodes
        // for each enemy node, do the vector math calculation
    //    for (i, enemy) in self.enemiesArray.enumerated() {
            //print("Moving enemy : \(i)")
            
        //  let a = self.player.position.x - enemy.position.x
         //   let b = self.player.position.y - enemy.position.y
         //   let d = sqrt(a*a + b*b)
         //   let xn = a / d
         //   let yn = b / d
            // @todo: need to move enemies left and right
            
          // let ENEMY_SPEED:CGFloat = 2
            // update position
         //   enemy.position.x = enemy.position.x + xn * ENEMY_SPEED
         //   enemy.position.y = enemy.position.y + yn * ENEMY_SPEED
    //    }
    }
    
  // func spawnEnemy()
//   {
    
  //  for _ in 1...4 {
 //      enemy = SKSpriteNode(imageNamed: "left1")
        
        //creating enemies randomnly
 //      let randX = Int(CGFloat(arc4random_uniform(UInt32(self.size.width-400))))
//    let randY = Int(CGFloat(arc4random_uniform(UInt32(self.size.height-400))))
        
  //      enemy.position = CGPoint(x:randX, y:randY)
        
 //        addChild(enemy)
 //   }
 //   }
    
    
    var lose:Bool = false
    
    func youLose() {
        self.lose = true
        
        print("YOU LOSE!")
        let messageLabel = SKLabelNode(text: "YOU LOSE!")
        messageLabel.fontColor = UIColor.yellow
        messageLabel.fontSize = 60
        messageLabel.position.x = self.size.width/2
        messageLabel.position.y = self.size.height/2
        addChild(messageLabel)
    }
    
}
